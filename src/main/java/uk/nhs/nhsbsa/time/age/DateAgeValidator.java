package uk.nhs.nhsbsa.time.age;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import uk.nhs.nhsbsa.time.types.Comparison;

class DateAgeValidator {

    /**
     * Support days, month, year granularity only.
     */
    private static final TemporalUnit[] units =
            new TemporalUnit[] {ChronoUnit.DAYS, ChronoUnit.MONTHS, ChronoUnit.YEARS};

    /**
     * Support standard EQ, LT, LTE, GTE, GT comparisons.
     */
    private Comparison comparison;

    /**
     * Age threshold defined as a Period.
     */
    private Period threshold;

    /**
     * Store granularity of threshold to allow date range comparisons in appropriate unit.
     */
    private TemporalUnit granularity;

    /**
     * Validator constructed with comparison and threshold.
     * 
     * @param comparison
     * @param threshold
     */
    public DateAgeValidator(Comparison comparison, Period threshold) {
        super();
        this.comparison = comparison;
        this.threshold = threshold;
        initialiseGranularity();
    }

    /**
     * Determine temporal granularity from threshold.
     */
    private void initialiseGranularity() {
        if (threshold.isZero()) {
            granularity = ChronoUnit.DAYS;
        } else {
            for (TemporalUnit temporalUnit : units) {
                if (threshold.get(temporalUnit) != 0) {
                    granularity = temporalUnit;
                    break;
                }
            }
        }
    }

    /**
     * Compares date argument with threshold to determine age.
     */
    public boolean isValid(LocalDate date) {
        if (date != null) {
            LocalDate now = LocalDate.now();
            long ageLimit;
            if (threshold.isZero()) {
                ageLimit = 0;
            } else {
                ageLimit = now.minus(threshold).until(now, granularity);
            }
            long age = date.until(now, granularity);
            return comparison.compare(age, ageLimit);
        }
        return true;
    }
}
