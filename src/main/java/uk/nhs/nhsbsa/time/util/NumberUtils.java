package uk.nhs.nhsbsa.time.util;

public class NumberUtils {

    private NumberUtils() {}

    /**
     * Parses string to integer returning null if not parsed.
     * 
     * @param value
     * @return
     */
    public static Integer toInteger(String value) {
        if (value != null) {
            try {
                return Integer.valueOf(value);
            } catch (NumberFormatException e) {
                // do nothing
            }
        }
        return null;
    }

}
