package uk.nhs.nhsbsa.time.util;

import java.util.Arrays;

public class StringUtils {

    private StringUtils() {}

    public static boolean isBlank(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static boolean anyBlank(String... values) {
        return Arrays.stream(values).anyMatch(StringUtils::isBlank);
    }

    public static boolean allBlank(String... values) {
        return values.length != 0 && Arrays.stream(values).allMatch(StringUtils::isBlank);
    }
}
