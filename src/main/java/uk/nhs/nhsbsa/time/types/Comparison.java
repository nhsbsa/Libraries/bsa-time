package uk.nhs.nhsbsa.time.types;

@SuppressWarnings({"rawtypes", "unchecked"})
public enum Comparison {

    LT {
        @Override
        public boolean compare(Comparable c1, Comparable c2) {
            return Comparison.nullSafeCompare(c1, c2) < 0;
        }
    },
    LTE {
        @Override
        public boolean compare(Comparable c1, Comparable c2) {
            return Comparison.nullSafeCompare(c1, c2) <= 0;
        }
    },
    EQ {
        @Override
        public boolean compare(Comparable c1, Comparable c2) {
            return Comparison.nullSafeCompare(c1, c2) == 0;
        }
    },
    GTE {
        @Override
        public boolean compare(Comparable c1, Comparable c2) {
            return Comparison.nullSafeCompare(c1, c2) >= 0;
        }
    },
    GT {
        @Override
        public boolean compare(Comparable c1, Comparable c2) {
            return Comparison.nullSafeCompare(c1, c2) > 0;
        }
    };

    private static final boolean NULL_GREATER = false;

    public abstract boolean compare(Comparable c1, Comparable c2);

    private static int nullSafeCompare(Comparable c1, Comparable c2) {
        int result;
        if (c1 == c2) {
            result = 0;
        } else if (c1 == null) {
            result = NULL_GREATER ? 1 : -1;
        } else if (c2 == null) {
            result = NULL_GREATER ? -1 : 1;
        } else {
            result = c1.compareTo(c2);
        }
        return result;
    }
}
