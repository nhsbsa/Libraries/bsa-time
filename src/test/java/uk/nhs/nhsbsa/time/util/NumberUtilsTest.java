package uk.nhs.nhsbsa.time.util;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

public class NumberUtilsTest {

    @Test
    public void toIntegerReturnsNullWhenNull() {
        assertThat(NumberUtils.toInteger(null)).isNull();
    }

    @Test
    public void toIntegerReturnsNullWhenNotParsable() {
        assertThat(NumberUtils.toInteger("")).isNull();
    }

    @Test
    public void toIntegerReturnsIntegerWhenNotNull() {
        assertThat(NumberUtils.toInteger("1")).isEqualTo(Integer.valueOf(1));
    }

}
