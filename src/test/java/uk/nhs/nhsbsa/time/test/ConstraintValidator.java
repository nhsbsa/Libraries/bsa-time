package uk.nhs.nhsbsa.time.test;

import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.hibernate.validator.HibernateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

public class ConstraintValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConstraintValidator.class);

    private static final LocalValidatorFactoryBean localValidatorFactory;
    static {
        localValidatorFactory = new LocalValidatorFactoryBean();
        localValidatorFactory.setProviderClass(HibernateValidator.class);
        localValidatorFactory.afterPropertiesSet();
    }

    public static <T> Set<ConstraintViolation<T>> validate(T object) {
        return localValidatorFactory.validate(object);
    }

    public static <T> boolean isValid(T object) {
        Set<ConstraintViolation<T>> constraintViolations = validate(object);
        boolean result = constraintViolations.isEmpty();
        if (!result) {
            LOGGER.info("Oject invalid: {}, {}", object, constraintViolations);
        }
        return result;
    }

    public static <T> boolean isValid(T object, String field) {
        Set<ConstraintViolation<T>> constraintViolations = validate(object);
        boolean result = !hasError(constraintViolations, field);
        if (!result) {
            LOGGER.info("Field '{}' invalid: {}, {}", field, object, constraintViolations);
        }
        return result;
    }

    public static <T> boolean hasError(Set<ConstraintViolation<T>> constraintViolations,
            String field) {
        if (constraintViolations == null) {
            return false;
        }
        Iterator<ConstraintViolation<T>> errors = constraintViolations.iterator();
        while (errors.hasNext()) {
            ConstraintViolation<?> error = errors.next();
            if (error.getPropertyPath().toString().equals(field)) {
                return true;
            }
        }
        return false;
    }
}
