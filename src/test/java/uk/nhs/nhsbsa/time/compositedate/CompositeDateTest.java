package uk.nhs.nhsbsa.time.compositedate;

import static org.assertj.core.api.Assertions.assertThat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import uk.nhs.nhsbsa.time.compositedate.CompositeDate;

public class CompositeDateTest {

    private static final String ALPHA = "A";
    private static final Integer YEAR = 2000;
    private static final Integer MONTH = 12;
    private static final Integer DAY = 31;
    private static final String S_YEAR = Integer.toString(YEAR);
    private static final String S_MONTH = Integer.toString(MONTH);
    private static final String S_DAY = Integer.toString(DAY);

    private CompositeDate date;

    @Before
    public void setup() {
        date = new CompositeDate();
    }

    @Test(expected = NullPointerException.class)
    public void shouldNPEWhenConstructedWithNullLocalDate() {
        new CompositeDate(null);
    }

    @Test
    public void shouldAllowConstructionWithNullIntegerFields() {
        new CompositeDate((Integer) null, (Integer) null, (Integer) null);
    }

    @Test
    public void shouldAllowConstructionWithNullStringFields() {
        new CompositeDate((String) null, (String) null, (String) null);
    }

    @Test
    public void shouldSetCompositeFieldsWhenConstructedWithLocalDate() {
        LocalDate localDate = LocalDate.of(YEAR, MONTH, DAY);
        date = new CompositeDate(localDate);
        assertThat(date.getYear()).isEqualTo(S_YEAR);
        assertThat(date.getMonth()).isEqualTo(S_MONTH);
        assertThat(date.getDay()).isEqualTo(S_DAY);
    }

    @Test
    public void shouldGetYearWhenSet() {
        date.setYear(S_YEAR);
        assertThat(date.getYear()).isEqualTo(S_YEAR);
    }

    @Test
    public void shouldGetMonthWhenSet() {
        date.setMonth(S_MONTH);
        assertThat(date.getMonth()).isEqualTo(S_MONTH);
    }

    @Test
    public void shouldGetDayWhenSet() {
        date.setDay(S_DAY);
        assertThat(date.getDay()).isEqualTo(S_DAY);
    }

    @Test
    public void toLocalDateShouldEqualFieldsWhenValid() {
        LocalDate expected = LocalDate.of(YEAR, MONTH, DAY);
        date.setYear(S_YEAR);
        date.setMonth(S_MONTH);
        date.setDay(S_DAY);
        assertThat(date.toLocalDate()).isEqualTo(expected);
    }

    @Test
    public void toLocalDateShouldReturnNullWhenNullYear() {
        date.setMonth(S_MONTH);
        date.setDay(S_DAY);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toLocalDateShouldReturnNullWhenNullMonth() {
        date.setYear(S_YEAR);
        date.setDay(S_DAY);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toLocalDateShouldReturnNullWhenNullDay() {
        date.setYear(S_YEAR);
        date.setMonth(S_MONTH);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toLocalDateShouldReturnNullWhenAlphaYear() {
        date.setYear(ALPHA);
        date.setMonth(S_MONTH);
        date.setDay(S_DAY);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toLocalDateShouldReturnNullWhenAlphaMonth() {
        date.setYear(S_YEAR);
        date.setMonth(ALPHA);
        date.setDay(S_DAY);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toLocalDateShouldReturnNullWhenAlphaDay() {
        date.setYear(S_YEAR);
        date.setMonth(S_MONTH);
        date.setDay(ALPHA);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toLocalDateShouldReturnNullWhen31April() {
        date = new CompositeDate(2000, 4, 31);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toLocalDateShouldReturnNullWhen29FebruaryInNonLeapYear() {
        date = new CompositeDate(2001, 2, 29);
        assertThat(date.toLocalDate()).isNull();
    }

    @Test
    public void toUtilDateShouldReturnNullWhenInvalid() {
        assertThat(date.toUtilDate()).isNull();
    }

    @Test
    public void toUtilDateShouldEqualFieldsWhenValid() {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(YEAR, MONTH - 1, DAY);
        Date expected = cal.getTime();
        date.setYear(S_YEAR);
        date.setMonth(S_MONTH);
        date.setDay(S_DAY);
        assertThat(date.toUtilDate()).isEqualTo(expected);
    }


    @Test
    public void toStringShouldReturnISO8601() {
        LocalDate expected = LocalDate.of(YEAR, MONTH, DAY);
        date.setYear(S_YEAR);
        date.setMonth(S_MONTH);
        date.setDay(S_DAY);
        assertThat(date.toString()).isEqualTo(expected.toString());
    }

    @Test
    public void equalShouldReturnTrueForSimilarFields() throws Exception {
        CompositeDate expected = new CompositeDate(LocalDate.of(YEAR, MONTH, DAY));
        date.setYear(S_YEAR);
        date.setMonth(S_MONTH);
        date.setDay(S_DAY);
        assertThat(date).isEqualTo(expected);
    }
}
