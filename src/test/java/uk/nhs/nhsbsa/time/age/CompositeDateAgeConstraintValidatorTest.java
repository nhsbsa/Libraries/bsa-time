package uk.nhs.nhsbsa.time.age;

import static uk.nhs.nhsbsa.time.types.Comparison.EQ;
import java.time.LocalDate;
import uk.nhs.nhsbsa.time.age.Age;
import uk.nhs.nhsbsa.time.age.CompositeDateAgeConstraintValidatorTest.AgeFixture;
import uk.nhs.nhsbsa.time.compositedate.CompositeDate;

public class CompositeDateAgeConstraintValidatorTest
        extends AbstractDateAgeConstraintValidatorTest<AgeFixture> {

    @Override
    protected void setDate(AgeFixture fixture, LocalDate date) {
        fixture.setCompositeDate(new CompositeDate(date));
    }

    @Override
    protected AgeFixture emptyFixture() {
        return new AgeFixture(new CompositeDate());
    }

    public class AgeFixture {

        @Age(comparison = EQ, threshold = "P1Y")
        private CompositeDate compositeDate;

        public AgeFixture(CompositeDate compositeDate) {
            super();
            this.compositeDate = compositeDate;
        }

        public CompositeDate getCompositeDate() {
            return compositeDate;
        }

        public void setCompositeDate(CompositeDate age) {
            this.compositeDate = age;
        }
    }

}
