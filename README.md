# BSA Dates Library

Library code to simplify capture, validation and storage of dates within NHS BSA projects.

## Features

* Composite Date and validation
* Age validation

## CompositeDate

A three part date object used for binding to UI form fields according to GDS user interface guidelines. `CompositeDate` provides convenience constructors and methods to transform from/to `java.util.Date` and `java.time.LocalDate`.

`CompositeDate` object comes with a JSR303 `ValidDate` annotation to ensure valid date entry. `ValidDate` exposes the `notBlank` attribute to enforce data entry.

_Dev note: `ValidCompositeDateConstraintValidator` needs improvement, as it doesn't identify errors on nested fields, when it could_

E.g.

```
@ValidDate(notBlank=true)
private CompositeDate requiredDate;

@ValidDate
private CompositeDate optionalDate;
```

## Age Validation

JSR303 `@Age` annotation to validate that a supported date (LocalDate/CompositeDate) is within the specified age constraint.

The annotation must define two attributes:

* `threshold` being the time threshold against which to validate, defined as an [ISO8601 Duration](https://en.wikipedia.org/wiki/ISO_8601#Durations). Use a negative duration to specify a future threshold
* `comparison` being how to compare the date with the threshold:
** Comparison.LT
** Comparison.LTE
** Comparison.EQ
** Comparison.GT
** Comparison.GTE

`@Age` can be used to validate `java.util.Date`, `java.time.LocalDate`, or `uk.nhs.nhsbsa.time.compositedate.CompositeDate`.

E.g.

```
@Age(comparison = Comparison.GTE, threshold = "P16Y")
private Date utilDate;

@Age(comparison = Comparison.GTE, threshold = "P16Y")
private LocalDate localDate;

@Age(comparison = Comparison.GTE, threshold = "P16Y")
private CompositeDate compositeDate;
```

`@Age.List` can be used to validate age ranges.

E.g.

```
@Age.List({
    @Age(comparison = Comparison.GTE, threshold = "P16Y"),
    @Age(comparison = Comparison.LTE, threshold = "P125Y")
})
private LocalDate localDate;
```

`@Age` can be used to validate more than a date of birth. It is validating against the duration between _now_ and the annotated date, so it can be used to check that the date is in the future, i.e. that age is less than zero. 

E.g.

```
@Age(comparison = LTE, threshold = "P0D")
private LocalDate futureDate;
```

